const batcher = ( requestFn ) => {

    var page   = 0;
    var buffer = [];
    var end    = false;

    return {

        ensureNext : async () => {

            if( buffer.length > 0 )
                return true;
            if( end )
                return false;
            
            var data = requestFn( page );
            buffer.push( ... data );
            page += 1;

            return !end;

        },

        next : () => {
            if( buffer.length === 0 )
                throw new Error( "Invalid next() call: no data to return." );

            return buffer.shift();
        }

    };

};

module.exports = { batcher };
